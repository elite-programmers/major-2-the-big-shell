#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>
#include <readline/readline.h>        
#include <readline/history.h>      




//global shell variables
char* shellVars[100];	//shell variables
char* shellVals[100];	//shell values
int shelVarCount = 0;	//shell variable counter

char* shellVarSubstitute(char *command);
char* variableSubstitute(char *s);
char* getShelVarVal(char *s);

/*debug driver
int main()
{
	while(1)
	{
		char* userInput;
		userInput = readline("$ ");
		userInput = shellVarSubstitute(userInput);
		printf("User input: %s\n", userInput);
	}
}
*/

char* shellVarSubstitute(char *command)
{
        int commandArrayLength;
	char* tokQuoteString[3];	//tokenized quote strings
        char quoteVal;
	char *commandToken;
        char *sub1;	//variable assignment substring 1
	char *sub2;	// variable assignment substing 2

		if (strstr(command, "="))//if users input contains an assignment statement
		{

			sub1 = strtok(command, "=");
			sub2 = strtok(NULL, "");
			
			int alreadyExists = 0;

			//scan threw each variable
			for(int i = 0; i < shelVarCount; i++)
			{
				if ((strcmp(shellVars[i], sub1)) == 0)	//if the variable in  sub1 already exist
				{
					shellVals[i] = sub2;	//assign it it's new value	
					alreadyExists = 1;		
				}
			}		
			
			if (alreadyExists != 1) //if the variable dosn't already exist
			{
				shellVars[shelVarCount] = sub1;
				shellVals[shelVarCount] = sub2;
				shelVarCount++;
				printf("string1: %s, string2: %s", shellVars[shelVarCount-1], shellVals[shelVarCount-1]);
			}
		
			char* newString = malloc(50);
			strcpy(newString, sub1);
			strcat(newString, "=");
			strcat(newString, sub2);
			return newString;
		}

		else if(strstr(command, "$")) //if needs variable substitution
		{
			return variableSubstitute(command);
		}

		else	//nothing needed from this function
			return command;
}

//recives a string
//calls "getShelVal" function 
//substitutes $ variable with it's value
char* variableSubstitute(char *s)
{
//	int i = 0;
	char *holder1;
	char *holder2;
	char *varNameHolder;
	char *newHolder;
	char *finalHolder;
	char *newString = malloc(50);
	strcpy(newString, " ");


	int length;

	printf("s[0] = %s\n", s[0]);

	if(s[0] == '$')  //if the first character is a $
	{
		strtok(s, "$"); //remove dollar sign
		varNameHolder = strtok(NULL, " ");  //tokenize variable
		holder2 = strtok(NULL, "");	//save the rest of command
		newHolder = getShelVarVal(varNameHolder);	//variable substituttion
		strcpy(finalHolder, newHolder);
		strcat(finalHolder, holder2);
		printf("finalHolder: %s\n", finalHolder);
		return finalHolder;
	}

	//tokenize string
	holder1 = strtok(s, "$");
	varNameHolder = strtok(NULL, " ");
	holder2 = strtok(NULL, "");
	newHolder = getShelVarVal(varNameHolder);
	
	printf("holder1: %s\n", holder1);
	printf("varName: %s\n", newHolder);
	printf("holder2: %s\n", holder2);

	if(holder1 != NULL)
	{
		newString = strcat(newString, holder1);
	}

	if(newHolder != NULL)
	{
		newString = strcat(newString, newHolder);
	}

	if(holder2 != NULL)
	{
		newString = strcat(newString, holder2);
	}


	printf("final hold: %s\n", newString);

	return newString;

	return newHolder;	
}

//get a shell variable's value(if it exists) from shellVars
char* getShelVarVal(char *s) 
{
	for (int i = 0; i < shelVarCount; i++)
	{
		if ((strcmp(s, shellVars[i])) == 0)
			return shellVals[i];
	}
}
