//Nelson Alfaro
//CSCE 3600 sec. 070
//ID: nsa0084
//Pipeline and Redirection

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>
#include <readline/readline.h>
#include <readline/history.h>


#define READ 0
#define WRITE 1

//unsigned char *command;
int main(){
	
	unsigned char *command;
	char *commandToken;
	char *theRest;
	int commandArrayLength;
	int fd[2];


	while(1){
		
		char *commandArray[255];
		command = readline("$ ");
	

		if(strlen(command) > 0)
		{
		
		add_history(command);
		
		}

		
		theRest = command;
		
		int i=0;
		while((commandToken = strtok_r(theRest, "| ", &theRest)))
		{
		
			commandArray[i] = commandToken;
			
			printf("Command:%s", commandArray[i]);
			printf("\n");
			//commandArrayLength = sizeof(commandArray)/sizeof(commandArray[0]);
			i++;
		}
	
		
					
		commandArrayLength = sizeof(commandArray)/sizeof(commandArray[0]);
		//malloc(sizeof(commandArray));

		
		//Loop through commandArray?
		pipe(fd);

		if(fork() == 0)
		{	
		close(fd[READ]);
		dup2(fd[WRITE], STDOUT_FILENO);
		execlp(commandArray[0], commandArray[0], NULL);  
		}
		else
		  //  if(fork() == 0)
		 //	{
			
			printf("In the Parent Process");
			close(fd[WRITE]);
			dup2(fd[READ], STDIN_FILENO);
			close(fd[READ]);
			execlp(commandArray[1], commandArray[1], NULL);
		//	}

		wait(NULL);
		//wait(NULL);


	}//while loop

return 0;

}//End of main