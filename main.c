#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <ctype.h>

#define READ 0
#define WRITE 1

//this function is used in the state machine for quotes to tokenize a line
char* Store(char *line,int count,int pos)
{
	char *str=(char*)malloc(count+1);
	int index=0;
	//malloc returns NULL
	if(str!=NULL)
	{
		//set array elements to \0
		for(int i=0;i<(count+1);i++)
		{
			str[i]='\0';
		}

		//create char to hold the outer quote
		char outer='\0';

		//loop through the string
		for(int i=0;i<count;i++)
		{
			//check if a quote has been stored
			if(outer!='\0')
			{
				//if current char is not one of the outer quotes
				if(line[pos]!=outer)
				{
					str[index]=line[pos];
					index++;
				}
				//if the current current char is one of the outer quotes
				else
				{
					//implement variable substitution
					//calculate total length of string with variable substituted
					//realloc str if length is too small

				}
			}
			//if the outer quote has not been stored
			else
			{
				//if the current char is a quote
				if(line[pos]=='\"' || line[pos]=='\'' || line[pos]=='`')
				{
					//store the current char as outer quote
					outer=line[pos];
				}
				//if the current char is not a quote
				else
				{
					str[i]=line[pos];
					index++;
				}
			}
			pos++;
		}
	}
	else
	{
		printf("malloc error\n");
		exit(1);
	}

	return str;

}
//this function counts the the total number of arguments in a command
int totalArgs(char *line)
{
	int state1=0,state2=0,state3=0,total=0;
	char match='\0';
		//loop through line
		for(int j=0;j<=strlen(line);j++)
		{
			if(j!=strlen(line))
			{
				//if the character is a whitespace an not inside quotes
				if(isspace(line[j]) && state3==0 )
				{
					//enter state 1
					state1=1;
					//exit state state 2 if applicable
					if(state2==1)
					{
						state2=0;
						//increment counter
						total++;
					}
				}
				//if the character is not a quote and not a whitespace
				else if(line[j]!='\'' && line[j]!='\"' && line[j]!='`')
				{
					//exit state 1 if applicable
					if(state1 == 1)
						state1=0;
					//enter state 2 if not already in state 3
					if(state3==0)
						state2=1;
				}
				//if the character is a quote
				else if(line[j]=='\'' || line[j]=='\"' || line[j]=='`')
				{
					//exit other states if applicable
					if(state1==1 || state2==1)
					{
						state1=0;
						state2=0;
					}

					//enter state 3
					state3=1;
					//if matching quote is found
					if(match == line[j])
					{
						//reset the matching quote
						match='\0';
						//exit state 3
						state3=0;
						//enter state 2
						state2=1;
					}
					//if a quote has not been stored yet
					else if(match=='\0')
					{
						match=line[j];
					}
				}
			}
			//if end of line
			else
			{
				//if in state 3
				if(state3==1)
				{
					printf("error: mismatched quotes\n");
					return -1;
				}
				else
				{
					total++;
				}
			}
		}
	return total;
}
//this function is the state machine described in the project document
char ** quote(char *line,int total)
{
	//allocate array
	char **argv=(char **)malloc(total *sizeof(char));
	int state1=0,state2=0,state3=0;
	char match='\0';
	int count=0,pos=-1;
	
	if(argv != NULL)
	{
		int i=0;

		//loop through line
		for(int j=0;j<=strlen(line);j++)
		{
			if(j!=strlen(line))
			{
				//if the character is a whitespace an not inside quotes
				if(isspace(line[j]) && state3==0 )
				{
					//enter state 1
					state1=1;

					//exit state state 2 if applicable
					if(state2==1)
					{
						state2=0;
						//store the string into argv
						argv[i]=Store(line,count,pos);

						//reset the character counter
						count=0;
						//reset the position
						pos=-1;
						//increment the argv counter
						i++;
					}
				}
				//if the character is not a quote and not a whitespace
				else if(line[j]!='\'' && line[j]!='\"' && line[j]!='`')
				{
					//exit state 1 if applicable
					if(state1 == 1)
						state1=0;

					//increment the character count variable
					count++;

					//store the starting position of argument
					if(pos==-1)
						pos=j;

					//enter state 2 if not already in state 3
					if(state3==0)
						state2=1;

				}
				//if the character is a quote
				else if(line[j]=='\'' || line[j]=='\"' || line[j]=='`')
				{
					//exit other states if applicable
					if(state1==1 || state2==1)
					{
						state1=0;
						state2=0;
					}

					//enter state 3
					state3=1;

					//if matching quote is found
					if(match == line[j])
					{
						//reset the matching quote
						match='\0';
						//exit state 3
						state3=0;
						//enter state 2
						state2=1;
					}
					//if a quote has not been stored yet
					else if(match=='\0')
					{
						match=line[j];
					}
					//increase the char count
					count++;
				}
			}
			//if end of line
			else
			{
				argv[i]=Store(line,count,pos);

			}
		}
	}
	else
	{
		printf("malloc error\n");
		exit(1);
	}
	return argv;

}

int main()
{
	unsigned char *command;
	char *commandToken;
	char *theRest;
	int commandArrayLength;
	int fd[2];
	
	
	while(1)
	{
		
		char *commandArray[255];
		command = readline("$ ");
	

		if(strlen(command) > 0)
		{
		
			add_history(command);
		
		}

		
		theRest = command;
				
		int i=0,total=0;
		//count the number of "commands" from input
		for(int j=0;j<(strlen(command));j++)
		{
			if(command[j]=='|')
				i++;
		}
		i++;
		//create array that holds the number of arguments for each "command"
		int Argc[i];
		printf("there are %d commands\n",i);
		
		//tokenize each command and store it
		char **Tokens[i];
		
		//loop through each command
		for(int j=0;j<i;j++)
		{
			commandToken = strtok_r(theRest, "| ", &theRest);

			//count the total number of arguments in a command
			int total = totalArgs(commandToken);
			Argc[j]=total;
			//determine if there was an error with quoting
			if(total != -1)
			{
				//store the arguments in array
				Tokens[j] = quote(commandToken,total);
			
				printf("Command:%s", Tokens[j][0]);
				printf("\n");
			}
			//exit for loop
			else
				break;
			
			printf("commands:");
			for(int k=0;k<total;k++)
			{
				printf("%s ",Tokens[j][k]);
			}
			printf("\n");
			
		}
	/*
		
					
		commandArrayLength = sizeof(commandArray)/sizeof(commandArray[0]);
		
		
		//Loop through commandArray?
		pipe(fd);

		if(fork() == 0)
		{	
			close(fd[READ]);
			dup2(fd[WRITE], STDOUT_FILENO);
			execlp(commandArray[0], commandArray[0], NULL);  
		}
		else
		  //  if(fork() == 0)
		 //	{
			
			printf("In the Parent Process");
			close(fd[WRITE]);
			dup2(fd[READ], STDIN_FILENO);
			close(fd[READ]);
			execlp(commandArray[1], commandArray[1], NULL);
		//	}

		wait(NULL);
		//wait(NULL);

*/
		//free the line read by readline
		free(command);
		//free memory required for quotes
		//loop through each command from input
		for(int l=0;l<i;l++)
		{
			//for each argument
			for(int m=0;m<Argc[l];m++)
			{
				free(Tokens[l][m]);
			}
			free(Tokens[l]);
		}//free()

	}//while loop

	return 0;
}


